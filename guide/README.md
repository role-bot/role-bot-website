---
title: Getting Started
---
# RoleBot

RoleBot is a highly customisable discord bot used to let people give roles to themselves so they can unlock specific parts of the community.  The main use case for this bot is for having an expandable community without users experiencing spam from other channels.

---- 
## Quick Start Guide
To get started [add the bot to your server][1] and list all the roles and their ID’s using:
```coffeescript
?role listall
```
Now you need to add a **Givable Role** which basically means a role which the user can give to himself.  This can be done by doing:
```coffeescript
?role create <role-id> <reference-name>
```
`role-id` is the ID of the role the user will be given.  
`reference-name` can be anything you want, and is what the user will use to give himself the role.

You have now created a role that the user can use.  For the user to acquire that role all they need to do is type:
```coffeescript
?roleme <reference-name>
```

### A more in-depth guide
For an in-depth guide and a rundown of all the features of the bot please visit the [guide index][2].

[1]:	https://discordapp.com/api/oauth2/authorize?client_id=472764196195401767&permissions=268512272&scope=bot "Add the bot to your server"
[2]:	guide_index.html
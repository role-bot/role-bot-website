---
title: Admin Roles
---

# What are admin roles?
Admin roles are given to the administrators that do not have **administrator** role and hence by default they can’t modify use admin commands.  These are useful when given to moderators so if they notice that there is something that needs instant attention it can be done without calling the owner or other administrators.  It is important to note that for people to use these commands, their role has to either include the **administrator** role, be part of the **admin roles** or be the owner of the server.

## What is an admin?
By default the owner and all the users with the `administrator` role are considered admins.  This can be turned off using [toggle admin override][1].  The owner is always considered as an admin and can’t be overwritten. 

### The hierarchy of admin roles
A role rank is the default hierarchy of roles in Discord.  The further down in the list the less the role rank is.  An admin can’t create or remove an admin role that is above his `role rank` for security reasons. 

---- 
### Who can use these command?
Only people considered as admin can use this command.

## Creating an admin role
To create an admin role use:
```js
?role admin add <role-id>
```
`role-id`  - The ID of the role to become admin

## Listing all the admin roles
To list all the admin roles use:
```js
?role admin list
```

## Removing an admin role
To remove an admin role use:
```js
?role admin delete <role-id>
```
`role-id`  - The ID of the role to be demoted from admin

## Toggle admin override
To toggle the admin override talked in [What is an admin?][2] use:
```js
?toggle admin override
```

[1]:	admin.html#toggle-admin-override
[2]:	admin.html#what-is-an-admin
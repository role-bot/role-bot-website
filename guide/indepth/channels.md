---
title: Bot Channels
---

# What are bot channels?
A bot channel is a channel where the bot can read and execute user/admin commands.  The bot gives the option to have two separate channels, one for user commands and the other for admin commands, but if you would like one channel for all commands the admin and user commands can be assigned to the same channel.  

### Why is this feature helpful
This feature is implemented so that all the bot action can be done in one channel rather than spamming all the other channels.

---- 
### Who can use these command?
Only people considered as admin can use this command.  For more information visit [Admin Roles][1]

## Listing all the channels
To list all the channels use:
```js
?listall channels
```

## Assign a bot channel
To assign a channel use:
```js
?assign channel <user/admin> <channel-id>
```
`user/admin` - Whether the assigned channel will be the admin or user channel
`channel-id` - The ID of the channel to be assigned to
(If the channel-id is `null` the bot un-assigns the  `user/admin` channel)

## Un-assign a bot channel
To un-assign a channel use:
```js
?unassign channel <user/admin>
```
`user/admin` - Whether the un-assigned channel will be the admin or user channel

[1]:	admin.html#What-is-an-admin
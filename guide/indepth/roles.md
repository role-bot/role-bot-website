---
title: Roles
---
# User Commands
A user can assign a role to himself, given that the admins have made the role givable.

### Who can use these command?
Everyone can use these commands.

## Listing all the givable roles
```js
?role list
```

## Getting a role
```js
?roleme <reference-name>
```
`role-name` - The reference name of the role

## Removing a role
```js
?role remove <role-name>
```
`role-name` - The reference name of the role

---- 
# Admin Commands
How an admin can add roles that can be assigned to the user.

### Who can use these command?
Only people considered as admin can use this command.  For more information visit [Admin Roles][1]

## Listing all the roles
```js
?listall roles
```
This commands list all the roles and their ID’s.

## Creating a role
```js
?role create <role-id> <reference-name> <category> 
```
`role-id` - The ID of the role to be added
`reference-name` - Can be anything you want, and is what the user will use to give himself the role.
[`category`][2] - This is an optional parameter.  If this is left empty or an invalid category is entered the role will be added without a category

## Deleting a role
```js
?role delete <reference-name>
```
`reference-name` - The reference name of the role to be deleted.
When a role is deleted the bot demotes all the users with that role.

## Modifying a role
```js
?role modify
```
This command can be used to modify: **reference name**, **description** and **category**

1. Once this command is called the admin is asked to enter the **role-id** of Givable Role to be modified.  
2. The admin is if they want to modify 
	1. **Reference Name**
	2. **Description**
	3. **Category**
3. They are then asked to enter the new data
	1. The description can have spaces in it

[1]:	admin.html#what-is-an-admin
[2]:	categories.html
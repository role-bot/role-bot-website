---
title: Prefix
---

# What is a prefix?
The prefix of the bot is the character or string used to call the bot.  By default this is `?` but can be changed to whatever the admins want.  An option has also been given to the admins to have a [space after prefix]().

Please note that once the prefix is changed from `?` to whatever the admins want consider all the commands in this tutorial to start with your prefix and not `?`

---- 

### Who can use these command?
Only people considered as admin can use this command.  For more information visit [Admin Roles][2]

## Changing the prefix
```js
?change prefix <new-prefix>
```
`new-prefix` - The new prefix.  Can’t have any spacing.

## Toggle space prefix
```js
?toggle prefix space
```
This **inverts the current status of the space after prefix**.  If it was off it will be turned on and if it was on it will be turned off.

[2]:	admin.html#what-is-an-admin
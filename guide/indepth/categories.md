---
title: Categories
---

# What are categories?
As a community gets bigger, one might need to have categories for a group of roles.  With categories one can split roles into sections such as Games, Voice or whatever the user desires. 

---- 
### Who can use these command?
Only people considered as admin can use this command.  For more information visit [Admin Roles][1]

## Listing all categories
To list all the categories use:
```js
?category list
```

## Creating the category
To create a category use:
```js
?category create <category-name>
```
`category-name` - The name of the category
To assign the category to an already created role one has to use [role modify command][2]

## Deleting a category
To delete a category use:
```js
?category delete <category-name>
```
`category-name` - The name of the category
When deleting a category all the roles with that category will be categorised as unassigned

[1]:	admin.html#what-is-an-admin
[2]:	roles.html#modifying-a-role
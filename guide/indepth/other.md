---
title: Other Commands
---

# What are other commands?
Other commands are commands that are implemented to ease the user/admin life when setting up and using RoleBot.

---- 

### Who can use these command?
Everyone can use these commands.

## Listing all the commands
```js
?list
```
This command is used to list all the other commands and show a basic description of them.  If the user is an admin all the admin commands are shown.

## Help on a specific command
```js
?help <command>
```
`command` - The command to be displayed help on
This command is used to list specific help on the `command`, giving a detailed description of each command

## Pinging the bot
```js
?ping
```
This command is used to check if the bot is online and the amount of ping between discord and the bot.
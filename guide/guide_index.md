# Guide Index
A guide index to help you sift through the commands

## Some Information
### What is a `Role-ID`?
A Role ID is what is used internally to identify roles.  This is used so that roles with the same name don’t get mixed up permissions.

### What is a `Channel-ID`?
A Channel ID is used internally to identify channels.  This is used so that channels with the same name don’t get mixed up permissions.

## Documentation Index
* [User Roles][1]
	* User commands
		* [Listing all the givable roles][2]
		* [Getting a role][3]
		* [Removing a role][4]
	* Admin commands
		* [Listing all the roles][5]
		* [Creating a role][6]
		* [Deleting a role][7]
		* [Modifying a role][8]

* [Admin Roles][9]
	* [What is an admin][10]
	* [Creating an admin role][11]
	* [Listing all the admin roles][12]
	* [Removing an admin role][13]
	* [Toggle admin override][14]

* [Categories][15]
	* [Creating a category][16]
	* [Listing all the categories][17]
	* [Deleting a category][18]

* [Bot channels][19]
	* [Assign a bot channel][20]
	* [Un-assign a bot channel][21]

* [Prefix][22]
	* [Changing the prefix][23]
	* [Toggle space prefix][24]

* [Other][25]
	* [Listing all the commands][26]
	* [Help on a specific command][27]
	* [Pinging the bot][28]

* [Practical Example][29]

[1]:	indepth/roles.html
[2]:	indepth/roles.html#Listing-all-the-givable-roles
[3]:	indepth/roles.html#Getting-a-role
[4]:	indepth/roles.html#Removing-a-role
[5]:	indepth/roles.html#Listing-all-the-roles
[6]:	indepth/roles.html#Creating-a-role
[7]:	indepth/roles.html#Deleting-a-role
[8]:	indepth/roles.html#Modifying-a-role
[9]:	indepth/admin.html
[10]:	indepth/admin.html#What-is-an-admin
[11]:	indepth/admin.html#Creating-an-admin-role
[12]:	indepth/admin.html#Listing-all-the-admin-roles
[13]:	indepth/admin.html#Removing-an-admin-role
[14]:	indepth/admin.html#Toggle-admin-override
[15]:	indepth/categories.html#
[16]:	indepth/categories.html#Creating-the-category
[17]:	indepth/categories.html#Listing-all-the-categories
[18]:	indepth/categories.html#Deleting-a-category
[19]:	indepth/channels.html#
[20]:	indepth/channels.html#Assign-a-bot-channel
[21]:	indepth/channels.html#Un-assign-a-bot-channel
[22]:	indepth/prefix.html#
[23]:	indepth/prefix.html#Changing-the-prefix
[24]:	indepth/prefix.html#Toggle-space-prefix
[25]:	indepth/other.html#
[26]:	indepth/other.html#Listing-all-the-commands
[27]:	indepth/other.html#Help-on-a-specific-command
[28]:	indepth/other.html#Pinging-the-bot
[29]:	indepth/example.html
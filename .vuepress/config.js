module.exports = {
    title: 'RoleBot',
    description: 'Documentation for the RoleBot Discord Bot',
    dest: './public',
    head: [
        ['link', { rel: 'icon', href: '/favicon.ico' }]
      ],
      themeConfig: {
        logo: "/icon.svg",
        nav: [
            {text: "Home", link: "/"},
            {text: "Guide", link: "/guide/"},
            {text: "Report a Bug", link: "https://gitlab.com/role-bot/bot/issues/new"}
        ],
        sidebar: {
            '/guide/': [
                '','guide_index',
                {
                    title: 'Guide',
                    children: [
                        'indepth/roles', 
                        'indepth/admin',
                        'indepth/categories',
                        'indepth/channels',
                        'indepth/prefix',
                        'indepth/other',
                        'indepth/example'
                    ]
                }
            ]
        }
      }
  }
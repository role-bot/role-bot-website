---
home: true
heroImage: /icon.svg
actionText: Get Started →
actionLink: /guide/
features:
- title: Simplicity First
  details: Simple to start using but offers a lot of customisability once so it suites your needs
- title: Performant and Stability
  details: Built with performance and stability in mind, so you can enjoy using the bot without any performance hiccups.
- title: Flexible
  details: Built from scratch on Java using the Discord4J Library so features can easily be implemented in the future.
footer: This bot is still work in progress
---